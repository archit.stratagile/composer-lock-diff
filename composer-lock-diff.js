#!/usr/bin/env node

const commander = require('commander');
const { readFile } = require('fs');

commander
  .version(require('./package.json').version)
  .option('-f --format <format>', 'Output format', 'plain')
  .arguments('<file-from> <file-to>')
  .parse(process.argv);

const composerDiff = async (fileFrom, fileTo) => {
  const parseFile = (file) => {
    const getPackages = (json) => {
      const mapPackages = (packages) => {
        const mapPackage = (pkg) => ({
          name: pkg.name,
          version: pkg.version,
        });

        return packages.map(mapPackage);
      };

      return [
        ...mapPackages(json.packages || []),
        ...mapPackages(json['packages-dev'] || []),
      ];
    };

    return new Promise((resolve, reject) => {
      readFile(file, (err, contents) => {
        if (err) {
          reject(err);
        }

        try {
          const json = JSON.parse(contents);

          resolve(getPackages(json));
        } catch (e) {
          reject(new Error(`${file}: ${e.message}`));
        }
      });
    });
  };

  try {
    const output = {};

    const [packagesFrom, packagesTo] = await Promise.all([
      parseFile(fileFrom),
      parseFile(fileTo),
    ]);

    const formatPlainText = (packages) => {
      const formatGroup = (group, title, formatOne) =>
        (group.length ? `${title}\n${group.map(formatOne).join('\n')}\n\n` : '');
      const formatOne = (pkg) => ` - ${pkg.name} (${pkg.version})`;
      const formatOneWithVersionChange = (pkg) =>
        ` - ${pkg.name} (${pkg.from} -> ${pkg.to})`;

      return `${formatGroup(packages.added, 'Packages added', formatOne)}\
${formatGroup(packages.removed, 'Packages removed', formatOne)}\
${formatGroup(packages.changed, 'Packages changed', formatOneWithVersionChange)}
`;
    };

    const findByName = (name) => (pkg) => name === pkg.name;

    const diffPackages = (packagesA, packagesB) =>
      packagesA.filter(
        (pkgA) => packagesB.findIndex(findByName(pkgA.name)) === -1,
      );

    const intersectPackages = (packagesA, packagesB) =>
      packagesA.filter(
        (pkgA) => packagesB.findIndex(findByName(pkgA.name)) !== -1,
      );

    const findChangedPackages = (packagesA, packagesB) =>
      intersectPackages(packagesA, packagesB)
        .filter(
          (pkgA) =>
            packagesB.find(findByName(pkgA.name)).version !== pkgA.version,
        )
        .map((pkgA) => ({
          name: pkgA.name,
          from: pkgA.version,
          to: packagesB.find(findByName(pkgA.name)).version,
        }));

    output.added = diffPackages(packagesTo, packagesFrom);
    output.removed = diffPackages(packagesFrom, packagesTo);
    output.changed = findChangedPackages(packagesFrom, packagesTo);

    const { format } = commander.opts();

    switch (format) {
      case 'json':
        console.info(output);
        break;
      case 'plain':
        console.info(formatPlainText(output));
        break;
      default:
        console.error(`Unknown output format \'${format}\'`);
    }
  } catch (e) {
    console.error(e.message);
    process.exit(e.code);
  }
};

const [fileFrom, fileTo] = commander.args;

if (!fileFrom || !fileTo) {
  commander.outputHelp(() => commander.help());

  process.exit(1);
}

composerDiff(fileFrom, fileTo);
